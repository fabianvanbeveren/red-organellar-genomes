#!/bin/sh

#you may have to unzip plastid_data/blast_results_tsv.7z
#to have the file plastid_data/all_genes_oneline_psiblast_orthAgogue.tsv
./mcl_to_og.py -m plastid_data/out.all.abc.I20l1i20 -b plastid_data/all_genes_oneline_psiblast_orthAgogue.tsv -o results/ -f 0.4 -i plastid_data/inparalogs.abc -s plastid_data/all_genes
