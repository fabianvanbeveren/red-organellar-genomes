#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 18:14:13 2018

@author: Fabian van Beveren
"""

#take the mcl --abc output to get the ortholog groups,
#select only those with a good % of species present
#and then deal with duplicates (2+ genes present of in genome in an OG)
#return alignments

#Libraries
from argparse import ArgumentParser
from datetime import datetime
from math import ceil
from collections import Counter
import os

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import SingleLetterAlphabet
from Bio.Seq import Seq
from csv import writer

#Arguments
parser = ArgumentParser(description="Return OGs without duplicates from mcl file")
parser.add_argument("-m", "--mcl", required = True, help = "the mcl output file (out.all.abc.I20l1i20)")
parser.add_argument("-b", "--blast", required = True, help = "blast file that was given to orthAgogue")
parser.add_argument("-o", "--output", default = None, help = "output directory")
parser.add_argument("-f", "--fraction", type = float, default = 0.5, help = "minimum fraction of species per OG [0.0 < f < 1.0; default 0.5]")
parser.add_argument("-i", "--inparalogs", required = True, help = "the inparalogs.abc file produced by orthAgogue")
parser.add_argument("-s", "--sequence", required = True, help = "file with all sequences that you blasted")
parser.add_argument("-v", "--verbose", default = False, action = "store_true", help = "be verbose")
parser.add_argument("--list", default = None, help = "if wanted, make list of all used proteins and their given OG to given filename")

#Parsing
args = parser.parse_args()

date = datetime.now().strftime("%y%m%d_%H%M%S")
outdir = args.output if args.output is not None else args.mcl + "_" + date

if args.verbose: print("Reading blast file")

#Read blast file
blast = []
with open(args.blast, "r") as blastfile:
	for line in blastfile:
		items = line.strip().split("\t")
		blast.append(items)

#colnames = ["qseqid", "sseqid", "pident", "length", "mismatch", "gapopen", "qstart", "qend", "sstart", "send", "evalue", "bitscore"]

if args.verbose: print("Reading mcl file")

#Read in mcl file as list
mcl_list = []
with open(args.mcl, "r") as mclfile:
	for line in mclfile:
		genes = line.strip().split()
		mcl_list.append(genes)

if args.verbose: print(genes)

#find inparalogs...		

inpquery = []
inpsubject = []
with open(args.inparalogs, "r") as inparafile:
	if len(line) > 1:
		for line in inparafile:
			query, subject = line.strip().split("\t")[0:2]
			inpquery.append(query)
			inpsubject.append(subject)
inparalogs = [inpquery, inpsubject]

	
#Count total number of UNIQUE species/taxa
#and count number of UNIQUE species/taxa per OG, then filter
species_list = []
num_species_per_OG = []

for OG in mcl_list:
	OG_species_list = []
	for gene in OG:
		species = gene.split("|")[0]
		#add species to our list of total unique species
		if species not in species_list:
			species_list.append(species)
		#add species to list of unique species for this OG
		if species not in OG_species_list:
			OG_species_list.append(species)
	#for each OG, count unique number and add to a list
	num_species_OG = len(set(OG_species_list))
	num_species_per_OG.append(num_species_OG)

#get number of total unique species, calculate the minimum number based on fraction
species_number = len(species_list)
all_species = sorted(species_list) #since I will overwrite species_list at some point, but want to keep this list...
minimum_species = ceil(species_number * args.fraction)

if args.verbose: print("Total number of species: " + str(species_number) + ". Minimum number: " + str(minimum_species))

#filter out OGs with low number
#and split them: groups that contain 1 gene per species, and those that have more genes in total then the number of unique species of that set...
mcl_list_sig_ok = [mcl_list[i] for i in range(len(mcl_list)) if num_species_per_OG[i] > minimum_species and num_species_per_OG[i] == len(mcl_list[i])]
mcl_list_sig_dups = [mcl_list[i] for i in range(len(mcl_list)) if num_species_per_OG[i] > minimum_species and num_species_per_OG[i] < len(mcl_list[i])]
		
#now the more difficult part: 
#for those in mcl_list_sig_dups we have duplicates, we need to find out why this duplicate is in there, and deal with it, in general:
#case 1: there are two sequences that are recent paralogs (i.e.) duplication after the last speciation in this group
#in this case we can pick one of them, doesn't matter too much, especially since they are often identical
#case 2: in one of the genomes the gene is split up in two (could be misannotation...)
#in this case we need to merge them
#case 3: anything else...

mcl_list_sig_undup = []
for OG in mcl_list_sig_dups:
	newOG = OG[:]
	#get just the species names
	species_list = []
	for gene in OG:
		species = gene.split("|")[0]
		species_list.append(species)
		
	#get only species that occur twice; saved into "dupl_sps"
	seen = []
	dupl_sps = []
	for species in species_list:
		if species not in seen:
			seen.append(species)
		elif species not in dupl_sps:
			dupl_sps.append(species)
	
	#now we can go through each species/genome and see what is up
	#first we collect the genes for each species, and get a subset of the blastresults with these
	#genes as query
	for species in dupl_sps:
		oldgenes = [gene for gene in OG if gene.find(species) != -1]
		
		#check duphits and see if they are highly similar, we can then keep just one (maybe based on blasthit with others...; case1)
		#we can simplify this by using the inparalogs.abc output from orthAgogue, which should include only those genes that
		#originate from recent duplication, and not from a split event (or more likely, a misannotation)
		genes = oldgenes[:]
		for gene in oldgenes:
			if gene in inparalogs[0]:
				loc = inparalogs[0].index(gene)
				inparalog = inparalogs[1][loc]
				if inparalog in genes:
					genes.remove(gene)
					
		#For sequence that remain, find "best" gene they all share
		#Note that here we assume they are all parts of one gene, which may not always be true
		#it will find a shared hit, and put them in order based on where they align to that hit
		if len(genes) > 1:
			
			blastsub = [hit for hit in blast if hit[0] in genes]
			
			hit_in_common = None
			genenumber = len(genes)
			seen = {}
			for row in blastsub:
				subject = row[1]
				if subject not in seen:
					seen[subject] = 1
				else:
					seen[subject] += 1
					if seen[subject] == genenumber:
						hit_in_common = subject
						break
		
			#For now, just start crying when no common gene is found
			if hit_in_common == None:
				if args.verbose: print(genes)
				if args.verbose:
					with open("blast_errorfile.tsv", "w") as FUCK:
						blastsadfile = writer(FUCK, delimiter = "\t")
						for row in blastsub:
							blastsadfile.writerow(row)
							
				#as they don't have hits in common, we just pick one of the other genes and see with which of the genes it has a hit
				for gene in OG:
					if gene not in genes:
						picked_gene = gene
						break
					
				for row in blastsub:
					if row[1] == gene:
						final_gene = row[0]
						
				
			else:
				blastcomp = [hit for hit in blastsub if hit[1] == hit_in_common]
				starts = [row[8] for row in blastcomp]
				queries = [row[0] for row in blastcomp]
				starts_order = [sorted(starts).index(i) for i in starts]
				ordered_queries = [queries[i] for i in starts_order]
				merged_gene = "\t".join(ordered_queries)
				#print(merged_gene)
				finalgene = merged_gene
		
			#at this point genes is just one value, that needs to be put in place of the duplicate values...
		else:
			finalgene = genes[0]
		
		#print(oldgenes)
		for gene in oldgenes:
			newOG.remove(gene)
		newOG.append(finalgene)
		
	mcl_list_sig_undup.append(newOG)
		
#now we can merge the new info with the old stuff and start collecting genes and making beautiful fasta files

#combine 
final_OGs = mcl_list_sig_ok + mcl_list_sig_undup
sorted_final_OGs = sorted(final_OGs, key=len, reverse = True)

names = []
for OG in sorted_final_OGs:
	genenames = []
	for gene in OG:
		genename = gene.split("|")[1].split("_")[0]
		genenames.append(genename)
	
	#get the genename most frequent in the group
	common_gene_name = Counter(genenames).most_common()[0][0]
	
	num = 1
	base = common_gene_name
	
	#if the picked gene name is already set, add a underscore+number to it
	while common_gene_name in names:
		#print("gene name already in use :(")
		num += 1
		common_gene_name = base + "_" + str(num)
	
	names.append(common_gene_name)

named_final_OGs = {}
for i in range(len(names)):
	named_final_OGs[names[i]] = sorted_final_OGs[i]

if args.list is not None:
	
	listfile = open(args.list, "w")
	 
	OG_num = 0
	for OG in named_final_OGs:
		OG_num += 1
		OG_numstr = str(OG_num).zfill(3)
		for gene in named_final_OGs.get(OG):
			listfile.write(gene + "\t" + OG_numstr + "\t" + OG + "\n") 

	listfile.close()

#get list of genes to attach sequence to	
all_genes = []	
for OG in named_final_OGs.values():
	for gene in OG:
		if gene.find("\t") == -1:
			all_genes.append(gene)
		else:
			genes = gene.split("\t")
			all_genes += genes

#add sequence to each, we should be able to go through file only once with the help of this dictionary :)
seqs = SeqIO.parse(args.sequence, "fasta")
gene_seq_dic = {}

for record in seqs:
	if record.id in all_genes:
		gene_seq_dic[record.id] = record.seq

#now the final step, collecting sequences and writing them to the files...
os.makedirs(outdir, exist_ok = True)
	
OG_num = 0
for OG in named_final_OGs:
	
	#filename
	OG_num += 1
	OG_numstr = str(OG_num).zfill(3)
	filename = outdir + "/OG_" + OG_numstr + "_" + OG + ".faa"
	OG_records = []
	
	#get items
	items = named_final_OGs[OG]
	for species in all_species:
		sp_items = [item for item in items if species + "|" in item]
			
		#one item (can not be more than one per species; in case of 0, do nothing...)
		if len(sp_items) == 1:
			gene = sp_items[0]
			
			#one gene in item
			if "\t" not in gene:
				sequence = gene_seq_dic[gene]
				record = SeqRecord(sequence, id = species, description = "")
				OG_records.append(record)
				
			
			#multiple genes in item
			#since they are listed in order in named_final_OGs gene names, this should give seq in order
			else:
				sequences = []
				for splitgene in gene.split("\t"):
					split_seq = str(gene_seq_dic[splitgene])
					sequences.append(split_seq)
				sequence = Seq("".join(sequences), SingleLetterAlphabet)
				record = SeqRecord(sequence, id = species, description = "")
				OG_records.append(record)
	
	#write record to file
	SeqIO.write(OG_records, filename, "fasta-2line")