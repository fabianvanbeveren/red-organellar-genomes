#!/bin/sh

./cds_from_exonerate.py -e test_data/Pkom.exonerate_ryo_intron600 -f test_data/Pkom_plastid.fa -g 11 --write_gff --includes_pi --show_location

#for clarity move produced files to "results" dir
#note that "tmp" file contains genes for which no good CDS could be found
mv tmp results/
mv test_data/Pkom.exonerate_ryo_intron600.truest_CDS.fa* results/

