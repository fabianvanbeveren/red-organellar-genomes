#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 18:04:14 2019

@author: fabian
"""

#as exonerate doesn't give valid ORFs (as in, it's good it takes into account the introns, but not start and stop codons)
#we need something that uses the exonerate data to find the matching gene if possible, doing three things:
#1.find a start codon near the beginning of the alignment if it isn't there
#2.find a stop codon near the end of the alignment
#3.make sure there is no stop codon in the alignment given

#since we need the alignment to easily identify this, we will have to read the file in verbose
#or set it up so it gives a useful output to us (i.e. we need the alignment and some statistics)

#all information should be contained in the vulgar format, except for stop-codons we will have to look for)

# imports
from Bio import SeqIO
from argparse import ArgumentParser

parser = ArgumentParser(description="Try to find complete CDS based on exonerate output, get output as CDS sequence or an updated gff file")
parser.add_argument("-f", "--fasta", required = True, help = "fasta file of genome")
parser.add_argument("-e", "--exonerate", required = True, help = 'results from exonerate [--verbose 0 --showalignment no --showvulgar no --ryo "#%%S %%V\n%%tcs"]')
parser.add_argument("-r", "--range", default = 30, type = int, help = "range to look for start/stop codon from alignment [default 30; MUST be divisible by 3]")
parser.add_argument("-g", "--genetic_code", default = 1, type = int, help = "genetic code to determine valid start/stop codons (for now 0 [ATG as only start], 1 [default] or 11)")
parser.add_argument("--ATG_priority", default = False, action = "store_true", help = "look for ATG before other start codons")
parser.add_argument("--write_gff", default = False, action = "store_true", help = "also write results to a gff file...")
parser.add_argument("--includes_pi", default = False, action = "store_true", help = 'use if you used --ryo "#%%S %%V %%pi\n%%tcs"')
parser.add_argument("--show_location", default = False, action = "store_true", help = "add start and end position of ALIGNMENT in fasta header")

args = parser.parse_args()

#filename of output
outfilename = args.exonerate + ".truest_CDS.fa"

#determine valid start/stop codons
#based on https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi
if args.genetic_code == 1:
	start_codons = ["ATG", "TTG", "CTG"]
	stop_codons = ["TAA", "TAG", "TGA"]
elif args.genetic_code == 0:
	start_codons = ["ATG"]
	stop_codons = ["TAA", "TAG", "TGA"]
elif args.genetic_code == 11:
	start_codons = ["ATG", "TTG", "CTG", "ATT", "ATC", "ATA", "GTG"] 
	stop_codons = ["TAA", "TAG", "TGA"]
elif args.genetic_code == 4:
	start_codons = ["TTA", "TTG", "CTG", "ATT", "ATC", "ATA", "ATG", "GTG"] 
	stop_codons = ["TAA", "TAG"]
else:
	raise Exception("Please pick genetic code 0, 1, 4, or 11, or add one to the script yourself")

#make vulgar info easily workable
#also makes sure that numerical values are integers and not string
def vulgar_to_list(vulgar):
	splitvulgar = vulgar.split(sep = " ")
	
	#if you want 
	vulgarend = 10 if args.includes_pi else 9
	
	sugar = [int(value) if value.isalnum() else value for value in splitvulgar[:vulgarend]]
	rest = [[splitvulgar[i], int(splitvulgar[i+1]), int(splitvulgar[i+2])] for i in range(vulgarend, len(splitvulgar), 3)]
	
	vulgar_list = [sugar, rest]
	return(vulgar_list)

#find start/stop codon
def get_codon_loc(contig_seq, position, _range, codons_oi, offset = 0):
	position -= offset
	start = position-_range
	end = position+_range+3
	
	seq = contig_seq[start:end]
	
	#some checks to make sure we don't go beyond start or end of contig
	if start < 0:
		attach = abs(start) * "X"
		seq = attach + seq
	if end > len(contig_seq):
		attach = (end - len(contig_seq)) * "X"
		seq = seq + attach
	
	codons = make_codon_list(seq)
	is_codon_oi = [codon in codons_oi for codon in codons]

	#the "most downstream" one we find, we choose, this works well for stop codons
	#for start_codons, you can use a loop that slowly increases the range (like I do in this script)
	if any(is_codon_oi):
		index = is_codon_oi.index(True)
		original_index = len(is_codon_oi)//2
		pos_difference = (index - original_index) * 3
		
		codon_loc = position + pos_difference + offset
		
	else:
		codon_loc = None
		
	return(codon_loc)

#split string up in pieces of three each, can change frame in relation to the sequence (but will cut off first nts)
def make_codon_list(sequence, frame = 0):
	codon_list = [sequence[i:i+3] for i in range(frame, len(sequence), 3)]
	if frame != 0:
		codon_list.insert(0, sequence[0:frame])
	return(codon_list)

#using the three rules of a "true cds" above, i.e. have start and stop codon, and no other stop codons
def is_true_CDS(cds_sequence, start_codons, stop_codons):
	has_start = cds_sequence[:3] in start_codons 
	has_stop = cds_sequence[-3:] in stop_codons
	has_inbetween_stop = any([codon in make_codon_list(cds_sequence[:-3]) for codon in stop_codons])
	
	return(has_start and has_stop and not has_inbetween_stop)

#to get reverse complement, needed when gene is on opposite strand; this version allows for ambigious codes as well

#revcompl = lambda x: ''.join([{'A':'T','C':'G','G':'C','T':'A'}[B] for B in x])
revcompl = lambda x: ''.join([{'A':'T','C':'G','G':'C','T':'A','Y':'R','R':'Y','W':'W','S':'S','K':'M','M':'K','D':'H','V':'B','H':'D','B':'V','N':'N','X':'X'}[B] for B in x])

# read in genome sequence
	
fasta_sequences = SeqIO.parse(args.fasta,'fasta')

fasta_dic = {}
for record in fasta_sequences:
	fasta_dic[record.id] = str(record.seq)
	
print("Number of contigs in genome file:", str(len(fasta_dic)))

### READ exonerate file###

#this is not a very neat method to do this, but I want to know the number of lines in the file...
def rawcount(filename):
    f = open(filename, 'rb')
    lines = 0
    buf_size = 1024 * 1024
    read_f = f.raw.read

    buf = read_f(buf_size)
    while buf:
        lines += buf.count(b'\n')
        buf = read_f(buf_size)

    return lines

totallines = rawcount(args.exonerate)


#count number of (in)correct CDSs
correct_CDS = 0
incorrect_CDS = 0

if args.write_gff:
	gfffile = open(outfilename + ".gff", "w")

outfile = open(outfilename, "w")

tmpfile = open("tmp", "w")

geneid_num = 0
exonid_num = 0
intronid_num = 0

with open(args.exonerate, "r") as exonerate_file:
	
	target_sequence = ""
	vulgar = ""
	firstline = True
	linenum = 0
	
	for line in exonerate_file:
		
		linenum += 1
		
		#don't start analysis at first line, as nothing has been parsed yet...
		if firstline:
			vulgar = line[1:].strip()
		
		#if line doesn't start with "#", it is part of a sequence, which we need (and we capitalise it with upper())
		elif not line.startswith("#"):
			target_sequence += line.strip().upper()
		
		#else, we have the vulgar information and the sequence, and we can start the analysis of this record
		if ((not firstline) and line.startswith("#")) or linenum == totallines:
			
			#get contig
			vulgar_list = vulgar_to_list(vulgar)
			contig_id = vulgar_list[0][4]
			contig_seq = fasta_dic[contig_id]

			#get reverse complement if needed
			if vulgar_list[0][7] == "-":
				contig_seq = revcompl(contig_seq)
				
			#get direction of gene
			startpos = vulgar_list[0][5]
			endpos = vulgar_list[0][6]
			
			if startpos > endpos:
				contig_seq = contig_seq[::-1]
				startpos = len(contig_seq) - startpos
				endpos = len(contig_seq) - endpos
				
			
			#find possible start codon
			#to make sure we don't look for the start codon within the intron...
			#(rough method, will also limit how far you look downstream of gene...)
			start_range = args.range if args.range <= vulgar_list[1][0][2] else vulgar_list[1][0][2] # - vulgar_list[1][0][2] % 3
			
			#start_loc = get_codon_loc(contig_seq, startpos, start_range, start_codons)
			
			#Not the best script, but easily works with get_codon_loc
			#First look for the nearest ATG if requested
			start_loc = None
			if args.ATG_priority:
				for i in range((start_range//3)+1):
					current_range = i*3
					start_loc = get_codon_loc(contig_seq, startpos, current_range, ["ATG"])
					if start_loc != None:
						break
			
			#Otherwise look for the nearest non-ATG start codon
			if start_loc is None:
				for i in range((start_range//3)+1):
					current_range = i*3
					start_loc = get_codon_loc(contig_seq, startpos, current_range, start_codons)
					if start_loc != None:
						break
			
			
			#find possible stop codon
			stop_range = args.range if args.range <= vulgar_list[1][-1][2] else vulgar_list[1][-1][2]
			stop_loc = get_codon_loc(contig_seq, endpos, stop_range, stop_codons, offset = 3)
			
			#get new sequence (if possible)
			query = vulgar_list[0][0]
			
			if stop_loc is None or start_loc is None:
				print("Can't find valid start and/or stop codon (query: " + query + ").")
				incorrect_CDS += 1
			
			else:			
				header = ">" + contig_id + "_" + query
				
				if args.show_location:
					if vulgar_list[0][7] == "-":
						header_startpos =  len(contig_seq) - startpos
						header_endpos = len(contig_seq) - endpos
					else:
						header_startpos = startpos
						header_endpos = endpos
					header += " " + str(header_startpos) + " " + str(header_endpos)
				
				newseq = ""
				#adjust for new start
				if start_loc >= startpos:
					loc_difference = start_loc - startpos
					newseq = target_sequence[loc_difference:]
				else:
					start_seq = contig_seq[start_loc:startpos]
					newseq = start_seq + target_sequence
				
				#adjust for new stop
				if stop_loc <= endpos:
					loc_difference = endpos - stop_loc
					if loc_difference != 0:
						newseq = newseq[:-loc_difference]

				else:
					end_seq = contig_seq[endpos:stop_loc]
					newseq += end_seq
			
				#final check, do we have a true CDS, including check for stop codon!
				if is_true_CDS(newseq, start_codons, stop_codons):
					correct_CDS += 1
					to_write = header + "\n" + newseq + "\n"	
					outfile.write(to_write)
					
					#write to gff file...
					#we can use the information of the vulgar format to write the exons ("M") and introns ("I")
					
					if args.write_gff:
						#write "gene" entry
						
						#reverse values for negative strand...
						_start_loc = start_loc if vulgar_list[0][7] == "+" else len(contig_seq) - stop_loc
						_stop_loc = stop_loc if vulgar_list[0][7] == "+" else len(contig_seq) - start_loc
						
						#adding some attributes...
						geneid_num += 1
						geneid = "ID=gene" + str(geneid_num)
						genename = "Name=" + query.split("|")[1]
						geneattr = ";".join([geneid, genename])
						
						geneline = "\t".join([contig_id, "exonerate:protein2genome:local-2CDS", "gene", str(_start_loc + 1), str(_stop_loc), str(vulgar_list[0][8]), vulgar_list[0][7], ".", geneattr])
						gfffile.write(geneline + "\n")
						
						location = start_loc + 1
						exon_length = 0
						exon_phase = 0
						for i in range(len(vulgar_list[1])):
							feature = vulgar_list[1][i]
				
							#features of this kind belong to exons
							if feature[0] in ["M", "G", "S"]:
								exon_length += feature[2]
							
							#if it is the first entry, we need to adjust it to new start position
							if i == 0:
								exon_length += startpos - start_loc
							
							#when we reach end of gene, or end of exon (i.e. start of intron)
							#we need to write the exon (and do some other things)
							if feature[0] == "I" or i == len(vulgar_list[1])-1:
								
								#adjust size if we are the end
								if i == len(vulgar_list[1])-1:
									exon_length += stop_loc - endpos
									
								#write the exon to file, and reset exon_length for next exon...
								end_location = location + exon_length - 1
								
								_location = location if vulgar_list[0][7] == "+" else len(contig_seq) - end_location + 1
								_end_location = end_location if vulgar_list[0][7] == "+" else len(contig_seq) - location + 1
								
								
								#exon attributes, can be useful
								exonid_num += 1
								exonid = "ID=exon" + str(exonid_num)
								exonparent = "Parent=gene" + str(geneid_num)
								exonattr = ";".join([exonid, exonparent]) 
								
								exonline = "\t".join([contig_id, "exonerate:protein2genome:local-2CDS", "exon", str(_location), str(_end_location), ".", vulgar_list[0][7], str(exon_phase), exonattr])
								gfffile.write(exonline + "\n")
								
								exon_phase = (exon_phase - exon_length) % 3 #calculates phase of next exon
								exon_length = 0
								location = end_location + 1
								
								#write the intron to file
								if feature[0] == "I":
									intron_length = feature[2] + 4 #splice sites on each side of intron are of length 2
									end_location = location + intron_length - 1
									
									_location = location if vulgar_list[0][7] == "+" else len(contig_seq) - end_location + 1
									_end_location = end_location if vulgar_list[0][7] == "+" else len(contig_seq) - location + 1
									
									intronid_num += 1
									intronid = "ID=intron" + str(intronid_num)
									intronparent = "Parent=gene" + str(geneid_num)
									intronattr = ";".join([intronid, intronparent]) 
									
									intronline = "\t".join([contig_id, "exonerate:protein2genome:local-2CDS", "intron", str(_location), str(_end_location), ".", vulgar_list[0][7], ".", intronattr])
									gfffile.write(intronline + "\n")
									location = end_location + 1	
				
				else:
					print("The CDS is not valid, check for an early stop codon (query: " + query + ").")
					incorrect_CDS += 1
					
					to_write = header + " " + str(start_loc) + " " + str(stop_loc) + "\n" + newseq + "\n"
					tmpfile.write(to_write)
		
			#prepare for next sequence...
			vulgar = line[1:].strip()
			target_sequence = ""
		firstline = False

print("Number of correct CDS found:", str(correct_CDS))
print("Number of incorrect CDS found:", str(incorrect_CDS))

if args.write_gff:
	gfffile.close()
outfile.close()
tmpfile.close()