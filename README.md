# Red Organellar Genomes

Custom python scripts used for the red algal organellar genome project are found here. Below is a quick overview of how they function.

## Python script to extract valid coding sequences based on protein alignments from Exonerate

Specific parameters are used to later easily extract the results from exonerate (v2.3), specifically needing the parameters `--verbose 0 --showalignment no --showvulgar no --ryo "#%S %pi %V\n%tcs"`

The script `cds_from_exonerate.py` is able to use these results to make valid CDSs (i.e. with valid start and stopcodons), all code and example files to run this script are found in [exonerate_coding_sequences](exonerate_coding_sequence). Manual refinement after is _necessary_.

## Python script to refine orthologous groups from orthAgogue & MCL

First run blast on all protein sequences, then run [orthAgogue](https://code.google.com/archive/p/orthagogue/) to estimate homology relations, and then run MCL to create homologous groups. With the python script `mcl_to_og.py`, the dataset can be refined to remove inparalogs and merge paralogs if they are split due to missannotation. Manual refinement afterwards is recommended. 

Specific formatting is necessary, see the script and example files in [refine_orthologous_groups](refine_orthologous_groups) for how it was used in this project.

### References:

Exonerate:
Slater, G. S. C., & Birney, E. (2005). Automated generation of heuristics for biological sequence comparison. BMC Bioinformatics, 6(1), 31. https://doi.org/10.1186/1471-2105-6-31

orthAgogue:
Ekseth, O. K., Kuiper, M., & Mironov, V. (2014). orthAgogue: An agile tool for the rapid prediction of orthology relations. Bioinformatics, 30(5), 734–736. https://doi.org/10.1093/bioinformatics/btt582
